#!/usr/bin/env python
import os
import shutil
from ansible.executor import task_queue_manager
from ansible.playbook import play
import base

fact_name = 'getversion.fact'
fact_file = os.path.abspath(
    os.path.join(os.path.dirname(__file__), fact_name))
tasks = [
    # add this fact to possible remotes
    {
        'action': {
            'module': 'copy',
            'args': {
                'src': fact_file,
                'dest': '/etc/ansible/facts.d/libre_version.fact',
                'mode': 0755
            }
        }
    }

]


def main():
    # local fact
    facts_dir = '/etc/ansible/facts.d/'

    if not os.path.exists(facts_dir):
        os.makedirs('/etc/ansible/facts.d/')
    shutil.copy(fact_file, facts_dir)
    os.chmod(os.path.join(facts_dir, fact_name), 0755)
    play_source = dict(name="test play", hosts='localhost', gather_facts='yes')
    # remote fact
    stufftodo = play.Play().load(
        play_source, variable_manager=base.variable_manager, loader=base.loader)
    tqm = None
    try:
        tqm = task_queue_manager.TaskQueueManager(
            inventory=base.inventory,
            variable_manager=base.variable_manager,
            loader=base.loader,
            options=base.options,
            passwords=base.passwords,
            # Use our custom callback instead of the ``default`` callback plugin
            stdout_callback=base.results_callback,
        )
        result = tqm.run(stufftodo)
        print(result)
    finally:
        if tqm is not None:
            tqm.cleanup()


if __name__ == '__main__':
    main()
